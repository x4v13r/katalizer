Feature: Account operations

    Scenario: Deposit
    In order to save money, As a bank client, I want to make a deposit in my account
        Given an account with balance of 0
        When client deposits 20
        Then account balance is 20

    Scenario: withdrawal
    In order to retrieve some or all of my savings As a bank client I want to make a withdrawal from my account
        Given an account with balance of 100
        When client withdraws 30
        Then account balance is 70

    Scenario: MultipleDeposits
        Given an account with balance of 100
        When client withdraws 30
        And client deposits 20
        Then account balance is 90

