package com.gitlab.x4v13r.bankApp;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Xavier on 28/06/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(strict=true, plugin = {"pretty", "html:target/cucumber"})
public class RunCukesTest {
}
