package com.gitlab.x4v13r.bankApp.domain;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Xavier on 22/06/2016.
 */
public class AccountTest {
    /**
     * US1: Feature Deposit
     * In order to save money, As a bank client, I want to make a deposit in my account
     * Scenario 1.1 :
     *     GIVEN a zero balanced account
     *     WHEN client deposit 20
     *     THEN account balance equals 20
     **/
    @Test
    public void test_deposit_of_twenty_should_involve_saving() {
        //GIVEN
        Account account = new Account();
        Assert.assertEquals(new Double(0), account.getBalance());
        //WHEN
        account.deposit(20d);
        //THEN
        Assert.assertEquals(new Double(20), account.getBalance());
    }

    /**
     * US2: Feature Withrawal
     * In order to retrieve some or all of my savings, As a bank client, I want to make a withdrawal from my account
     * Scenario 2.1 :
     *      Given a 100 balanced account
     *      When client withdraw 30
     *      Then balance is 70
     */
    @Test
    public void test_withraw_of_twenty_should_decrease_balance() {
        //GIVEN
        Account account = new Account();
        account.deposit(100d);
        Assert.assertEquals(new Double(100), account.getBalance());
        //WHEN
        account.withdraw(30d);
        //THEN
        Assert.assertEquals(Double.valueOf(70), account.getBalance());
    }

}
