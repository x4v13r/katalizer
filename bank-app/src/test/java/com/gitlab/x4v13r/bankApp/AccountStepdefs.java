package com.gitlab.x4v13r.bankApp;

import com.gitlab.x4v13r.bankApp.domain.Account;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
/**
 * Created by Xavier on 23/06/2016.
 */

public class AccountStepdefs {

    private Account account = null;

    @Given("^an account with balance of (\\d+)$")
    public void an_account_with_balance_of(double amount) throws Throwable {
        account = new Account();
        if(amount>0) {
            account.deposit(amount);
        }
        if(amount<0) {
            account.withdraw(-amount);
        }
    }

    @When("^client deposits (\\d+)$")
    public void client_deposit(double amount) throws Throwable {
        account.deposit(amount);
    }
    @When("^client withdraws (\\d+)$")
    public void client_withdraws(double amount) throws Throwable {
        account.withdraw(amount);
    }

    @Then("^account balance is (\\d+)$")
    public void account_balance_is(double amount) throws Throwable {
        Assert.assertEquals(Double.valueOf(amount), account.getBalance());
    }
}
