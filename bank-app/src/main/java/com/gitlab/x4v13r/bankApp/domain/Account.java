package com.gitlab.x4v13r.bankApp.domain;

/**
 * Created by Xavier on 20/06/2016
 */
public class Account {
    public Double getBalance() {
        return balance;
    }

    private Double balance = 0d;

    public Double deposit(Double amount){
        balance = balance + amount;
        return balance;
    }
    public Double withdraw(Double amount){
        balance = balance - amount;
        return balance;
    }

}
